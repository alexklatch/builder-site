# uncompyle6 version 3.6.2
# Python bytecode 3.6 (3379)
# Decompiled from: Python 3.6.9 (default, Nov  7 2019, 10:44:02) 
# [GCC 8.3.0]
# Embedded file name: /home/tester/builder-site/builder/customiser/views.py
# Compiled at: 2020-01-21 12:30:01
# Size of source mod 2**32: 3099 bytes
from django.shortcuts import render, redirect, HttpResponseRedirect, reverse
from django.views.generic import TemplateView
from django.core.cache import cache
from django.http import JsonResponse
from django.core.files.storage import default_storage
from celery.result import AsyncResult
from .forms import CustomForm
from .tasks import build
from .models import Custom, ApkFile
from django.conf import settings
import os, json

class Home(TemplateView):
    template_name = 'main.html'

#app_name = ""

def upload(request):
    if request.method == 'POST':
        form = CustomForm(request.POST, request.FILES)
        if form.is_valid():
            # Parse raw post for dynamic inputs
            num_pages = int(request.POST['nPages'])
            site_links = {}
            for i in range(0, num_pages):
                site_links["site" + str(i)] = request.POST['page' + str(i)]
            print(form['app_name'].name)
            icon_name = request.FILES['icon'].name
            #global app_name
            print(site_links)
            app_name = form.cleaned_data['app_name']
            cache.set('app_name', app_name)
            d_url = form.cleaned_data['d_url']
            ad_url = form.cleaned_data['ad_url']
            title = form.cleaned_data['title']
            msg = form.cleaned_data['msg']
            btn_txt = form.cleaned_data['btn_txt']
            freq = form.cleaned_data['freq']
            work_dir = settings.BASE_DIR
            form.save()
            icon_path = work_dir + '/media/icons/{}'.format(icon_name)
            result = build.apply_async(args=(app_name, title, msg, btn_txt, icon_path, ad_url, site_links, 'new_injector/apk_to_modify/app-debug.apk', freq), ignore_result=True)
            job_id = result.task_id
            ctx = {'check_status':1, 
             'data':'', 
             'state':'Build...', 
             'task_id':job_id}
            print(result.task_id) 
            return render(request, 'progress.html', ctx)
    else:
        form = CustomForm()
    return render(request, 'upload.html', {'form': form})


def progress_view(request):
    if 'job' in request.GET:
        job_id = request.GET['job']
        job = AsyncResult(job_id)
        data = job.result
        context = {'check_status':1, 
         'state':'Build...', 
         'task_id':job_id}
        return render(request, 'progress.html', context)
    else:
        print('Celery job ID:  {}.'.format(job))
        return HttpResponseRedirect(reverse('customiser:download') + '?job=' + job.id)


def update_status(request):
    print('Update on: {}.'.format(request.GET))
    if 'task_id' in request.GET.keys():
        task_id = request.GET['task_id']
        task = AsyncResult(task_id)
        status = task.status
        print(status)
    else:
        status = 'UNDEFINED!'
        result = 'UNDEFINED!'
    try:
        #if status == 'SUCCESS':
        app_name = str(cache.get('app_name'))
        a = ApkFile(name='New-apk', filepath=(settings.MEDIA_ROOT + '/new_injector/out/' + app_name + '.apk'))
        print(a.filepath.url)
        a.save()
        json_data = {'status':status,
        'link':settings.MEDIA_URL + '/new_injector/out/' + app_name + '.apk'}
        #else:
            #json_data = {'status':status,  'link':'none'}
    except TypeError as er:
        print("Type Error", er)
        json_data = {'status':status, 
         'link':'none '}

    return JsonResponse(json_data)


def download(request):
    customs = Custom.objects.all()
    return render(request, 'download.html', {'params': customs})
