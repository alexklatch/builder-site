# Create your tasks here
from __future__ import absolute_import, unicode_literals
from new_injector.src.customiser import customiser
from builder.celery import app 

from celery import shared_task, current_task, task

import time

@task(name = "build", bind = True)
def build(self, app_name, title, main_txt, btn_txt, icon_path, dl, site_links, apk_path, freq):
    result = 0
    c = customiser(app_name=app_name, title=title, main_txt=main_txt, btn_txt=btn_txt, icon_path=icon_path, dls=site_links, adl=adl, freq=freq)
    c.build(apk_path)
    current_task.update_state(
        state="PROGRESS",
        meta={
            'status': 'BUILDING...',
            }
        )
    result = 100
    return result

#build('test', 'test', 'test', 'test', '/home/tester/builder-site/builder/new_injector/icons/iconAndroid.png', 'test', 'test', 'app-debug.apk')
