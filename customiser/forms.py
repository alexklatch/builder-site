from django import forms

from .models import Custom

class CustomForm(forms.ModelForm):
    class Meta:
        model = Custom
        fields = ('app_name', 'd_url', 'ad_url', 'title', 'msg', 'btn_txt', 'icon', 'freq')
