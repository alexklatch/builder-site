from django.urls import path, re_path, include
from django.conf import settings
from django.conf.urls.static import static

from . import views

app_name = 'customiser'

urlpatterns = [
    path('', views.Home.as_view(), name='main'),
    path('customiser/upload/', views.upload, name='upload'),
    path('customiser/download', views.progress_view, name='download'),
    re_path(r'^[\s\S]*/update_status/$', views.update_status, name='update_status'),
    #re_path(r'^(?P<task_id>[\w-]+)/$', views.get_progress, name='task_status'),
    #path('^ajax/check_progress/$', views.check_progress, name='check_progress')
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)