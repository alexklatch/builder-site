from django.db import models

# Create your models here.

class Custom(models.Model):
    app_name = models.CharField(max_length=100)
    d_url = models.CharField(max_length=100)
    ad_url = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    msg = models.CharField(max_length=100)
    btn_txt = models.CharField(max_length=100)
    icon = models.FileField(upload_to='icons/') 
    freq = models.CharField(max_length=100)

    def __str__(self):
        return self.app_name


class ApkFile(models.Model):
    name = models.CharField(max_length=500)
    filepath= models.FileField(upload_to='files/', null=True, verbose_name="")

    def __str__(self):
        return self.name + ": " + str(self.filepath)