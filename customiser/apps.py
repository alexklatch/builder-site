from django.apps import AppConfig


class CustomiserConfig(AppConfig):
    name = 'customiser'
